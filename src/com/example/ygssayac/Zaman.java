package com.example.ygssayac;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

public class Zaman {

public static String Now() 	{
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh-mm-ss");
		
	    String strTime = sdf.format(now);
	    System.out.println("Time: " + strTime);
	    return strTime;
	}
	
	public static Date convertToDate(String tarih_zaman) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh-mm-ss");
		ParsePosition pos = new ParsePosition(0);
		Date d = dateFormat.parse(tarih_zaman, pos);
		return d;
	}

	public static long Subtract(String dt1, String dt2)
	{
		long l1 = convertToDate(dt1).getTime();
		long l2 = convertToDate(dt2).getTime();
	
		return (l1-l2); /// 1000;
	}
}